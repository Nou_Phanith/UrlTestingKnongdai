package com.example.demo.ModelUrl;

public class Model {

    private String typeUrl;
    public Model(String typeUrl) {
        this.typeUrl = typeUrl;
    }

    public String getTypeUrl() {
        return typeUrl;
    }

    public void setTypeUrl(String typeUrl) {
        this.typeUrl = typeUrl;
    }

    @Override
    public String toString() {
        return "Model{" +
                "typeUrl='" + typeUrl + '\'' +
                '}';
    }
}
