package com.example.demo.ServiceUrl;

import java.io.IOException;
import java.net.MalformedURLException;

public interface interfaceService {

    String getResponseCode(String urlString) throws MalformedURLException, IOException;

}
