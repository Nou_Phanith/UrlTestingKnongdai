package com.example.demo.ServiceUrl;


import com.example.demo.RepositoryUrl.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@org.springframework.stereotype.Service
public class Service implements interfaceService{

    Repository repository;
    Service service;
    public Service(Repository repository) {
        this.repository = repository;
    }
    @Override
    public String getResponseCode(String urlString) throws MalformedURLException, IOException {
        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.connect();
        System.out.print("GetResponeMessage: ");
        String mss = httpURLConnection.getResponseMessage();
        System.out.println(mss);
        return this.repository.getResponseCode(urlString);
    }

}
