package com.example.demo.ControllerUrl;

import com.example.demo.ServiceUrl.Service;
import com.sun.tracing.dtrace.ModuleAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;

@org.springframework.stereotype.Controller
public class Controller {
    Service service;
    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    @GetMapping("/index")
    public String displayIndex(Model model){
        model.addAttribute("url", new String());
        return "index";
    }

    @GetMapping("/error404")
    public String error(@ModelAttribute("url") String url) throws IOException {
        String getStatus;
        getStatus = this.service.getResponseCode("https://"+url);
        System.out.println(getStatus);
        return "success200";
    }
}
